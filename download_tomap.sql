create table icdsnom_u
	(
		icd_code varchar (255),
		icd_name varchar (255),
		match_score number,
		concept_id  number
	);

WbImport -file=/home/ekorchmar/proc_stand/icd10ptosnomed.csv
         -type=text
         -table=ICDSNOM_U
         -encoding="UTF-8"
         -header=true
         -decode=false
         -dateFormat="yyyy-MM-dd"
         -timestampFormat="yyyy-MM-dd HH:mm:ss"
         -delimiter='\t'
         -quotechar='"'
         -decimal=.
         -fileColumns=ICD_CODE,ICD_NAME,MATCH_SCORE,CONCEPT_ID,$wb_skip$
         -quoteCharEscaping=none
         -ignoreIdentityColumns=false
         -deleteTarget=true
         -continueOnError=false
         -batchSize=1000;
drop table manual_check;
create table manual_check as
with icd10p as
	(
		select concept_code, concept_id, concept_name
		from concept
		where 
			vocabulary_id = 'ICD10PCS' and
			length (concept_code) = 6
	),
heart as
	(
		select procedure_id 
		from mappings
		group by procedure_id
		having count (distinct snomed_id) <= 3
	)

select 
	o.concept_code as icd_code,
	o.concept_id as icd_id,
	o.concept_name as icd_name,
	m.rel_id,
	o2.concept_id as snomed_id,
	o2.concept_code as snomed_code,
	o2.concept_name as snomed_name
from icd10p o
left join mappings m on
	o.concept_id = m.procedure_id
left join concept o2 on
	o2.concept_id = m.snomed_id
left join heart h on
	h.procedure_id = m.procedure_id
where
	m.rel_id is null or
	h.procedure_id is not null

	UNION 

select
	c2.concept_code as icd_code,
	c2.concept_id as icd_id,
	c2.concept_name as icd_name,
	'USAGI' as rel_id,
	c.concept_id as snomed_id,
	c.concept_code as snomed_code,
	c.concept_name as snomed_name
from icdsnom_u u
join concept c on
	c.concept_id = u.concept_id and
	match_score >= 0.75
join concept c2 on
	c2.vocabulary_id = 'ICD10PCS' and
	c2.concept_code = substr (u.icd_code,1,6) and
	length (c2.concept_code) = 6
;
create table icd10_actual 
	(
		concept_id number,
		concept_code varchar (15),
		"count" number
	);
WbImport -file=/home/ekorchmar/ICD10PCS_actual.csv
         -type=text
         -table=ICD10_ACTUAL
         -encoding="UTF-8"
         -header=true
         -decode=false
         -dateFormat="yyyy-MM-dd"
         -timestampFormat="yyyy-MM-dd HH:mm:ss"
         -delimiter='|'
         -quotechar='"'
         -decimal=.
         -fileColumns=CONCEPT_ID,CONCEPT_CODE,"count"
         -quoteCharEscaping=none
         -ignoreIdentityColumns=false
         -deleteTarget=true
         -continueOnError=false
         -batchSize=1000;


;
select distinct m.ICD_CODE,m.ICD_ID,m.ICD_NAME,m.REL_ID,m.SNOMED_ID,m.SNOMED_CODE,m.SNOMED_NAME from manual_check m
join ICD10_ACTUAL a
on
	substr(a.concept_code,1,6) = m.icd_code